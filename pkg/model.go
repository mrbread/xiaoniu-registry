package pkg

/**
存放实体类
*/

// Service 服务对象
type Service struct {
	// 服务名称
	Name string `json:"name"`
	// 健康实例数
	HealthCnt int `json:"healthCnt"`
	// 注册实例数
	RegistryCnt int `json:"registryCnt"`
}

// Instance 实例对象
type Instance struct {
	// 服务名称
	ServiceName string `json:"serviceName"`
	// ip
	Ip string `json:"ip"`
	// 端口
	Port string `json:"port"`
	// 权重
	Weight int `json:"weight"`
	// 是否健康
	IsHealth bool `json:"isHealth"`
	// 下线操作时将当前的权重保存至该变量当中
	PrevWeight int
}
