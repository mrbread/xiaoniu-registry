package cache

import (
	"errors"
	"log"
	"time"
	"xiaoniu-registry/pkg"
)

/**
此文件主要为了保存程序运行时的所有服务和实例信息
暂时不使用数据库来存储信息
所有信息都保存到内存中

这肯定不是一个好办法，但是目前我们先这样
*/

// SERVICES 存放所有的服务
var SERVICES = make([]*pkg.Service, 0)

// key 服务名 value是具体的服务对象
var serviceMap = make(map[string]*pkg.Service)

// AddNewService 添加服务
func AddNewService(serviceName string) (bool, error) {
	if serviceName == "" {
		return false, errors.New("服务名不能为空")
	}
	// 如果该服务已经存在
	_, ok := serviceMap[serviceName]
	if ok {
		return true, nil
	}
	var newService = &pkg.Service{
		Name:        serviceName,
		HealthCnt:   0,
		RegistryCnt: 0,
	}
	SERVICES = append(SERVICES, newService)
	// 将该服务名称添加到map里面
	serviceMap[serviceName] = newService

	return true, nil
}

// DeleteService 删除服务
func DeleteService(serviceName string) (bool, error) {
	_, ok := serviceMap[serviceName]
	if !ok {
		return true, nil
	}
	// 从map里面删除该服务名称
	delete(serviceMap, serviceName)
	// 从切片中删除该服务信息
	for i, service := range SERVICES {
		if service.Name == serviceName {
			SERVICES = append(SERVICES[:i], SERVICES[i+1:]...)
			return true, nil
		}
	}
	return true, errors.New("没有该服务名称需要删除")
}

// QueryServiceList 查询服务列表
func QueryServiceList() []*pkg.Service {
	return SERVICES
}

// key为服务名 value为该服务下所有的实例
var instanceMap = make(map[string][]*pkg.Instance)

// 通过实例去获取它属于哪个服务
var instanceToServiceMap = make(map[string][]string)

// 存放所有实例的map
var healthMap = make(map[string]*pkg.Instance)

// AddInstance 添加实例
func AddInstance(serviceName, ip, port string, weight int) (bool, error) {
	if !check(serviceName, ip, port) {
		return false, errors.New("参数错误")
	}
	// 如果服务不存在，先添加服务
	_, ok := serviceMap[serviceName]
	if !ok {
		AddNewService(serviceName)
	}

	var key = ip + ":" + port
	services, ok := instanceToServiceMap[key]
	if ok {
		flag := false
		for _, service := range services {
			if service == serviceName {
				flag = true
				break
			}
		}
		if !flag {
			services = append(services, serviceName)
			instanceToServiceMap[key] = services
		}
	} else {
		services = make([]string, 0)
		services = append(services, serviceName)
		instanceToServiceMap[key] = services
	}
	// 新建一个实例
	var newInstance = &pkg.Instance{
		ServiceName: serviceName,
		Ip:          ip,
		Port:        port,
		Weight:      weight,
		IsHealth:    true,
	}

	instances, f := instanceMap[serviceName]
	if f {
		flag := false
		for _, ins := range instances {
			if ins.Ip == ip && ins.Port == port {
				flag = true
				break
			}
		}
		if !flag {
			instances = append(instances, newInstance)
			instanceMap[key] = instances
		}
	} else {
		instances = make([]*pkg.Instance, 0)
		instances = append(instances, newInstance)
		instanceMap[serviceName] = instances
	}
	// 加入到健康检测
	AddEntity(serviceName, ip, port)
	putToHealthMap(serviceName, ip, port, newInstance)

	serviceMap[serviceName].HealthCnt++
	serviceMap[serviceName].RegistryCnt++

	log.Printf("add the instance %s, port is %s, serviceName is %s", ip, port, serviceName)

	return true, nil
}

// DeleteInstance 添加实例
func DeleteInstance(serviceName, ip, port string) (bool, error) {
	if !check(serviceName, ip, port) {
		return false, errors.New("参数错误")
	}
	var key = ip + ":" + port
	cur, ok := instanceToServiceMap[key]
	if !ok {
		return true, nil
	}

	var newCur = make([]string, 0)
	for _, c := range cur {
		if c != serviceName {
			newCur = append(newCur, c)
		}
	}

	if len(newCur) != 0 {
		// 说明该实例还属于其他服务
		instanceToServiceMap[key] = newCur
	} else {
		// 该实例不属于任何一个服务了，直接删除key
		delete(instanceToServiceMap, key)
	}

	// 删除服务下面的该实例
	instances := instanceMap[serviceName]
	if len(instances) == 1 {
		// 删除该服务
		DeleteService(serviceName)
		delete(instanceMap, serviceName)
	} else {
		var newInstances = make([]*pkg.Instance, 0)
		for _, ins := range instances {
			if ins.Ip != ip || ins.Port != port {
				newInstances = append(newInstances, ins)
			}
		}
		instanceMap[serviceName] = newInstances
	}

	// 从健康检测中移除
	RemoveEntity(serviceName, ip, port)
	removeFromHealthMap(serviceName, ip, port)

	if _, ok = serviceMap[serviceName]; ok {
		serviceMap[serviceName].RegistryCnt--
	}
	log.Printf("remove the instance %s, port is %s, serviceName is %s", ip, port, serviceName)
	return true, nil
}

// QueryInstancesByServiceName 根据服务名来查询该服务下面的实例列表
func QueryInstancesByServiceName(serviceName string) []*pkg.Instance {
	instances, ok := instanceMap[serviceName]
	if !ok {
		instances = make([]*pkg.Instance, 0)
	}
	return instances
}

// ManualOfflineInstance 手动下线某实例，即将该实例的权重置为0
func ManualOfflineInstance(serviceName, ip, port string) (bool, error) {
	key := serviceName + ":" + ip + ":" + port
	instance, ok := healthMap[key]
	if ok && instance.Weight != 0 {
		// 将当前实例的权重保存在PrevWeight变量中
		instance.PrevWeight = instance.Weight
		// 将权重置为0
		instance.Weight = 0
	}
	return true, nil
}

// ManualOnlineInstance 手动上线某实例，即将该实例的权重重置为上一次的权重
func ManualOnlineInstance(serviceName, ip, port string) (bool, error) {
	key := serviceName + ":" + ip + ":" + port
	instance, ok := healthMap[key]
	if ok {
		if instance.PrevWeight != 0 {
			instance.Weight = instance.PrevWeight
		}
	}
	return true, nil
}

// UpdateInstanceToUnhealthy 将该实例置为不健康的
func UpdateInstanceToUnhealthy(serviceName, ip, port string) {
	key := serviceName + ":" + ip + ":" + port
	instance, ok := healthMap[key]
	if ok {
		instance.IsHealth = false
		serviceMap[serviceName].HealthCnt--
	}
}

// key := serviceName + ":" + ip + ":" + port value为具体的实例
func putToHealthMap(serviceName, ip, port string, instance *pkg.Instance) {
	key := serviceName + ":" + ip + ":" + port
	_, ok := healthMap[key]
	if !ok {
		healthMap[key] = instance
	}
}
func removeFromHealthMap(serviceName, ip, port string) {
	key := serviceName + ":" + ip + ":" + port
	_, ok := healthMap[key]
	if ok {
		delete(healthMap, key)
	}
}

// 校验参数是否正确
func check(serviceName, ip, port string) bool {
	if serviceName == "" || ip == "" || port == "" {
		return false
	}
	return true
}

// 保存所有还在维持心跳的实例，30秒
var healthInstance = make(map[string]*Entity)

func UpdateLastBeatTime(serviceName, ip, port string) {
	key := serviceName + ":" + ip + ":" + port
	_, ok := healthInstance[key]
	// 假设一个实例很久之后掉线了，但是后面又发送心跳过来了，我们需要重新注册服务，添加实例，默认权重为1
	if !ok {
		// 添加实例
		AddInstance(serviceName, ip, port, 1)
		return
	}
	healthInstance[key].LastBeatTime = time.Now()
	// 如果之前是不健康的状态
	if !healthMap[key].IsHealth {
		healthMap[key].IsHealth = true
		// 对应服务的健康实例数也得加一
		serviceMap[serviceName].HealthCnt++
	}

}

// AddEntity 将该实例添加到healthInstance这个map里面
func AddEntity(serviceName, ip, port string) {
	key := serviceName + ":" + ip + ":" + port
	_, ok := healthInstance[key]
	if ok {
		healthInstance[key].LastBeatTime = time.Now()
		return
	}
	healthInstance[serviceName+":"+ip+":"+port] = &Entity{
		ServiceName:  serviceName,
		Ip:           ip,
		Port:         port,
		LastBeatTime: time.Now(),
	}
}

// RemoveEntity 将该实例从healthInstance这个map里面删除
func RemoveEntity(serviceName, ip, port string) {
	key := serviceName + ":" + ip + ":" + port
	_, ok := healthInstance[key]
	if ok {
		delete(healthInstance, serviceName+":"+ip+":"+port)
	}
}

type Entity struct {
	ServiceName string
	Ip          string
	Port        string
	// 记录上一次心跳的时间
	LastBeatTime time.Time
}

// FIRST 将超过15秒没有发送心跳过来的实例置为不健康的
const FIRST = 15

// SECOND 将超过30秒没有发送心跳过来的实例直接删除
const SECOND = 30

// DoBackGroundJob 随着程序一起启动，每15秒中执行一次，校验实例状态是否正常
func DoBackGroundJob() {
	for {
		// fmt.Println("========开始执行健康检测任务=========")
		// 遍历所有的实例列表
		for _, v := range healthInstance {
			if time.Now().Sub(v.LastBeatTime).Seconds() > SECOND {
				// 剔除该实例
				DeleteInstance(v.ServiceName, v.Ip, v.Port)
			} else if time.Now().Sub(v.LastBeatTime).Seconds() > FIRST {
				// 更新该实例为不健康状态
				UpdateInstanceToUnhealthy(v.ServiceName, v.Ip, v.Port)
			}
		}
		// fmt.Println("========健康检测任务结束=========")
		// 休眠15秒
		time.Sleep(time.Second * FIRST)
	}

}
