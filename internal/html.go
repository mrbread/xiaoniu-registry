package internal

import (
	"html/template"
	"log"
	"net/http"
)

func IndexHandler(w http.ResponseWriter, req *http.Request) {
	t, err := template.ParseFiles("frontend/template/index.html")
	if err != nil {
		log.Println("转换模板报错：", err)

	}
	err = t.Execute(w, nil)
	if err != nil {
		log.Println("渲染模板报错：", err)
	}
}

func InstanceHtmlHandler(w http.ResponseWriter, req *http.Request) {
	t, err := template.ParseFiles("frontend/template/instance.html")
	if err != nil {
		log.Println("转换模板报错：", err)

	}
	err = t.Execute(w, nil)
	if err != nil {
		log.Println("渲染模板报错：", err)
	}
}
