package service

import (
	"encoding/json"
	"net/http"
	"xiaoniu-registry/internal/cache"
)

// ListHandler 获取服务列表
func ListHandler(w http.ResponseWriter, req *http.Request) {
	serviceList := cache.QueryServiceList()
	res, err := json.Marshal(serviceList)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("json转换异常"))
	} else {
		w.Write(res)
	}
}
