package main

import (
	"fmt"
	"log"
	"net/http"
	"xiaoniu-registry/internal"
	"xiaoniu-registry/internal/cache"
	"xiaoniu-registry/internal/heartbeat"
	"xiaoniu-registry/internal/instance"
	"xiaoniu-registry/internal/service"
)

/**
程序启动入口
*/
func main() {
	fmt.Println("======== Welcome to the xiaoniu-registry ｜ 菜牛注册中心，监听端口为：9898 =========")
	// 心跳接口
	http.HandleFunc("/api/beat", heartbeat.BeatHandler)

	// 查询服务列表
	http.HandleFunc("/api/service/list", service.ListHandler)

	// 查询服务下面的实例列表
	http.HandleFunc("/api/instance/list", instance.ListHandler)
	// 新增实例
	http.HandleFunc("/api/instance/add", instance.AddInstanceHandler)
	// 删除实例
	http.HandleFunc("/api/instance/delete", instance.DeleteInstanceHandler)
	// 手动下线实例
	http.HandleFunc("/api/instance/offline", instance.ManualOfflineHandler)
	// 手动上线实例
	http.HandleFunc("/api/instance/online", instance.ManualOnlineHandler)

	// 渲染前端
	http.HandleFunc("/", internal.IndexHandler)
	http.HandleFunc("/instance", internal.InstanceHtmlHandler)
	http.Handle("/js/", http.StripPrefix("/js/", http.FileServer(http.Dir("frontend/template/js"))))

	// 启动健康检测定时任务
	go cache.DoBackGroundJob()

	log.Fatal(http.ListenAndServe(":9898", nil))
}
