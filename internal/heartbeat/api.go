package heartbeat

import (
	"log"
	"net/http"
	"xiaoniu-registry/internal/cache"
)

// BeatHandler
//处理微服务实例客户端发过来的心跳，证明该服务是正常的，也就是健康状态
//如果一段时间内收不到该微服务的心跳，先把该实例置为不健康状态，暂定为15秒
//如果更长时间内收不到该微服务实例的心跳，那么直接将该实例从所在服务下面的实例中删除
//如果某个服务下面没有实例了，那么整个服务应该也要从服务列表中被剔除
//
//NOTICE：Nacos中有永久实例的概念，后续如果有空再实现。
// GET /api/beat?serviceName=&ip=&port=
func BeatHandler(w http.ResponseWriter, req *http.Request) {
	// 从请求体中获取三个参数并校验
	var serviceName = req.URL.Query().Get("serviceName")
	var ip = req.URL.Query().Get("ip")
	var port = req.URL.Query().Get("port")
	if !check(serviceName, ip, port) {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("参数缺失"))
	} else {
		log.Printf("receive the heartbeat message from %s, port is %s, serviceName is %s", ip, port, serviceName)
		// 更新实例的状态
		cache.UpdateLastBeatTime(serviceName, ip, port)
		w.Write([]byte("发送成功"))
	}
}

// 校验参数是否正确
func check(serviceName, ip, port string) bool {
	if serviceName == "" || ip == "" || port == "" {
		return false
	}
	return true
}
