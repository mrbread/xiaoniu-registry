package instance

import (
	"encoding/json"
	"net/http"
	"strconv"
	"xiaoniu-registry/internal/cache"
)

// ListHandler 查询某个服务下面的实例列表
func ListHandler(w http.ResponseWriter, req *http.Request) {
	var serviceName = req.URL.Query().Get("serviceName")
	if serviceName == "" {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("服务名不存在！"))
		return
	}
	instanceList := cache.QueryInstancesByServiceName(serviceName)
	res, err := json.Marshal(instanceList)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("json转换异常"))
	} else {
		w.Write(res)
	}
}

// AddInstanceHandler 新增实例
func AddInstanceHandler(writer http.ResponseWriter, req *http.Request) {
	var serviceName = req.URL.Query().Get("serviceName")
	var ip = req.URL.Query().Get("ip")
	var port = req.URL.Query().Get("port")
	var weight = req.URL.Query().Get("weight")

	// 权重
	w := 1
	if weight != "" {
		if a, err := strconv.Atoi(weight); err == nil {
			w = a
		}
	}

	instance, err := cache.AddInstance(serviceName, ip, port, w)
	if instance {
		writer.Write([]byte("新建实例成功"))
	} else {
		writer.WriteHeader(http.StatusInternalServerError)
		writer.Write([]byte(err.Error()))
	}
}

// DeleteInstanceHandler 删除实例
func DeleteInstanceHandler(writer http.ResponseWriter, req *http.Request) {
	var serviceName = req.URL.Query().Get("serviceName")
	var ip = req.URL.Query().Get("ip")
	var port = req.URL.Query().Get("port")

	instance, err := cache.DeleteInstance(serviceName, ip, port)
	if instance {
		writer.Write([]byte("删除实例成功"))
	} else {
		writer.WriteHeader(http.StatusInternalServerError)
		writer.Write([]byte(err.Error()))
	}
}

// ManualOfflineHandler 手动下线某实例
func ManualOfflineHandler(writer http.ResponseWriter, req *http.Request) {
	var serviceName = req.URL.Query().Get("serviceName")
	var ip = req.URL.Query().Get("ip")
	var port = req.URL.Query().Get("port")

	instance, err := cache.ManualOfflineInstance(serviceName, ip, port)
	if instance {
		writer.Write([]byte("手动下线成功"))
	} else {
		writer.WriteHeader(http.StatusInternalServerError)
		writer.Write([]byte(err.Error()))
	}
}

// ManualOnlineHandler 手动上线某实例
func ManualOnlineHandler(writer http.ResponseWriter, req *http.Request) {
	var serviceName = req.URL.Query().Get("serviceName")
	var ip = req.URL.Query().Get("ip")
	var port = req.URL.Query().Get("port")

	instance, err := cache.ManualOnlineInstance(serviceName, ip, port)
	if instance {
		writer.Write([]byte("手动上线成功"))
	} else {
		writer.WriteHeader(http.StatusInternalServerError)
		writer.Write([]byte(err.Error()))
	}
}
