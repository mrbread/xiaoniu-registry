// 获取url的参数
function getUrlParamValue(name) {
    if (name == null || name == 'undefined') {return null; }
    let searchStr = decodeURI(location.search);
    let infoIndex = searchStr.indexOf(name + "=");
    if (infoIndex == -1) { return null; }
    let searchInfo = searchStr.substring(infoIndex + name.length + 1);
    let tagIndex = searchInfo.indexOf("&");
    if(tagIndex != -1) { searchInfo = searchInfo.substring(0, tagIndex); }
    return searchInfo;
};

// 根据服务名获取实例列表
function getInstanceListByServiceName(serviceName){
    let req;
    if (window.XMLHttpRequest)
    {
        req=new XMLHttpRequest();
    }
    else
    {
        req=new ActiveXObject("Microsoft.XMLHTTP");
    }
    req.onreadystatechange=function()
    {
        if (req.readyState==4 && req.status==200)
        {
            const resp = req.responseText;
            const obj = JSON.parse(resp)
            let html = "<table class=\"table table-info table-striped\"><tr>\n" +
                "    <th>服务名</th>\n" +
                "    <th>实例IP</th>\n" +
                "    <th>实例端口</th>\n" +
                "    <th>权重</th>\n" +
                "    <th>是否健康</th>\n" +
                "    <th>操作</th>\n" +
                "  </tr>";
            for(let i = 0; i < obj.length; i++){
                html+="<tr>\n" +
                    "    <td>"+obj[i].serviceName+"</td>\n" +
                    "    <td>"+obj[i].ip+"</td>\n" +
                    "    <td>"+obj[i].port+"</td>\n" +
                    "    <td>"+obj[i].weight+"</td>\n" +
                    "    <td>"+obj[i].isHealth+"</td>\n" +
                    "    <td><button type=\"button\" class=\"btn btn-sm btn-danger\" onclick='offline(\""+obj[i].serviceName+"\",\""+obj[i].ip+"\",\""
                    +obj[i].port+"\")'>手动下线</button>" +
                    "        <button type=\"button\" class=\"btn btn-sm btn-primary\" onclick='online(\""+obj[i].serviceName+"\",\""+obj[i].ip+"\",\""
                    +obj[i].port+"\")'>手动上线</button></td>\n" +
                    "  </tr>";
            }
            html +="</table>"
            document.getElementById("instance").innerHTML = html;
        }
    }
    req.open("GET","/api/instance/list?serviceName="+serviceName,false);
    req.send();
}

function search(){
    let serviceName = getUrlParamValue("serviceName");
    document.getElementById("name").innerHTML = "当前服务名为："+serviceName
    getInstanceListByServiceName(serviceName);
}

// 手动下线
function offline(serviceName, ip, port){
    let req;
    if (window.XMLHttpRequest)
    {
        req=new XMLHttpRequest();
    }
    else
    {
        req=new ActiveXObject("Microsoft.XMLHTTP");
    }
    req.onreadystatechange=function()
    {
        if (req.readyState==4 && req.status==200)
        {
            const resp = req.responseText;
            if( resp != "") {
                alert(resp)
            }
        }
    }
    req.open("GET","/api/instance/offline?serviceName="+serviceName+"&ip="+ip+"&port="+port,false);
    req.send();
}

// 手动上线
function online(serviceName, ip, port){
    let req;
    if (window.XMLHttpRequest)
    {
        req=new XMLHttpRequest();
    }
    else
    {
        req=new ActiveXObject("Microsoft.XMLHTTP");
    }
    req.onreadystatechange=function()
    {
        if (req.readyState==4 && req.status==200)
        {
            const resp = req.responseText;
            if( resp != "") {
                alert(resp)
            }
        }
    }
    req.open("GET","/api/instance/online?serviceName="+serviceName+"&ip="+ip+"&port="+port,false);
    req.send();
}

// 页面加载成功之后
window.onload = function (){
    search();
};

setInterval(search, 5000);
