function getServiceList(){
    let req;
    if (window.XMLHttpRequest)
    {
        //  IE7+, Firefox, Chrome, Opera, Safari 浏览器执行代码
        req=new XMLHttpRequest();
    }
    else
    {
        // IE6, IE5 浏览器执行代码
        req=new ActiveXObject("Microsoft.XMLHTTP");
    }
    req.onreadystatechange=function()
    {
        if (req.readyState==4 && req.status==200)
        {
            const resp = req.responseText;
            const obj = JSON.parse(resp)
            let html = "<table class=\"table table-info table-striped\"><tr>\n" +
                "    <th>服务名</th>\n" +
                "    <th>健康实例数</th>\n" +
                "    <th>注册实例数</th>\n" +
                "    <th>操作</th>\n" +
                "  </tr>";
            for(let i = 0; i < obj.length; i++){
                html+="<tr>\n" +
                    "    <td>"+obj[i].name+"</td>\n" +
                    "    <td "+(obj[i].healthCnt != 0? "":"style='background: red;'")+" >"+obj[i].healthCnt+"</td>\n" +
                    "    <td>"+obj[i].registryCnt+"</td>\n" +
                    "    <td><a href='/instance?serviceName="+obj[i].name+"' target='_blank'>查看实例</a></td>\n" +
                    "  </tr>";
            }
            document.getElementById("service").innerHTML = html;
        }
    }
    req.open("GET","/api/service/list",false);
    req.send();
}

setInterval(getServiceList, 5000);
